<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
  <nav class="navbar navbar-expand-sm navbar-default">

    <div class="navbar-header">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
      </button>
      <a class="navbar-brand" href="./"><img src="<?php echo base_url(); ?>assets/logo.png" alt="Logo"></a>
      <a class="navbar-brand hidden" href="./"><img src="<?php echo base_url(); ?>assets/images/logo2.png" alt="Logo"></a>
    </div>

    <div id="main-menu" class="main-menu collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active">
          <a href="<?php echo base_url(); ?>"> <i class="menu-icon fa fa-dashboard"></i>ทั่วไป </a>
        </li>
        <li>
          <a href="<?php echo base_url('withdraw'); ?>"> <i class="menu-icon fa fa-sign-out"></i> เบิก-จ่าย </a>
        </li>
        <li>
          <a href="<?php echo base_url('#'); ?>"> <i class="menu-icon fa fa-exchange"></i> ยืม - คืน </a>
        </li>
        <li>
          <a href="<?php echo base_url('welcome/genBarcode/'); ?>"> <i class="menu-icon fa fa-barcode"></i> สร้าง Barcode </a>
        </li>

        <h3 class="menu-title">จัดการทั่วไป</h3><!-- /.menu-title -->
        <li>
          <a href="<?php echo base_url('usersetting');?>"> <i class="menu-icon fa fa-list"></i> กำหนดสิทธิ์การใช้งาน </a>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-stack-exchange"></i> วัสดุ/ครุภัณฑ์</a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-th"></i><a href="<?php echo base_url('stock/addStock'); ?>"> เพิ่ม </a></li>
            <li><i class="fa fa-pencil-square-o"></i><a href="<?php echo base_url('stock/edit'); ?>"> แก้ไข - ลบ</a></li>
            <li><i class="fa fa-list-alt"></i><a href="<?php echo base_url('stock/report'); ?>"> คงเหลือ</a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i> ผู้ใช้งาน</a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-user-plus"></i><a href="<?php echo base_url('account'); ?>"> เพิ่ม </a></li>
            <li><i class="fa fa-pencil-square-o"></i><a href="<?php echo base_url('account/management') ?>"> แก้ไข - ลบ</a></li>
          </ul>
        </li>
        <?php if(empty($this->dataLogin['userID'])): ?>
          <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
          <li class="menu-item-has-children dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
            <ul class="sub-menu children dropdown-menu">
              <li>
                <i class="menu-icon fa fa-sign-in"></i><a href="<?php echo base_url('authen'); ?>">Login</a>
              </li>
              <li><i class="menu-icon fa fa-sign-in"></i><a href="<?php echo base_url('authen/regis') ?>">Register</a></li>
              <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
            </ul>
          </li>
        <?php endif; ?>

      </ul>
    </div><!-- /.navbar-collapse -->
  </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->
