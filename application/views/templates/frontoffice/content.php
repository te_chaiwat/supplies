<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title> <?php echo $this->app->getTitle(); ?></title>
	<meta name="description" content="Supplies CS/IT UDRU">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico">

	<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-3.3.1.min.js"></script>

	<?php $this->load->view('templates/frontoffice/_css'); ?>
</head>
<body>

	<?php $this->load->view('templates/frontoffice/leftMenu'); ?>

	<!-- Right Panel -->

	<div id="right-panel" class="right-panel">
		<?php $this->load->view('templates/frontoffice/topMenu'); ?>
		<!-- Header-->

		<div class="content mt-3">

			<?php echo $this->app->getLayout(); ?>

		</div> <!-- .content -->
	</div><!-- /#right-panel -->

	<!-- Right Panel -->

	<?php $this->load->view('templates/frontoffice/_js'); ?>

</body>
</html>
