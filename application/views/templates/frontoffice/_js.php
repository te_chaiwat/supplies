
<script>

  var url = "<?php echo base_url(); ?>";
  var ctl = "<?php echo $this->router->fetch_class(); ?>"
</script>

<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<!-- Bootstrap DataTable --><!-- Bootstrap pdfmake.min -->
<script src="<?php echo base_url(); ?>assets/plugins/DataTable/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTable/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTable/Buttons-1.5.1/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/DataTable/pdfmake-0.1.32/vfs_fonts.js"></script>


    <script>
      function include(file) {
        var script = document.createElement('script');
        script.src = file;
    // script.type = 'text/javascript';
    // script.defer = true;

    document.getElementsByTagName('body').item(0).appendChild(script);

}


function messageAlert(message) {  //function alert message
    // $('#messageAlert').modal('show');
    var html = '<div class="modal fade" id="messageAlert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
    html += '<div class="modal-dialog" role="document">';
    html += '<div class="modal-content modal-sm">';
    html += '<div class="modal-header">';
    html += '<h5 class="modal-title" id="exampleModalLabel">แจ้งเตือน !</h5>';
    html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    html += ' <span aria-hidden="true">&times;</span>';
    html += '</button>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<span class="row mx-auto clearfix"><i class="fa fa-info-circle fa-2x text-info" aria-hidden="true"> </i><h4> &nbsp;&nbsp;'+message +'</h4></span>';
    html += '</div>';
    // html += '<div class="modal-footer">';
    // html += '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
    // html += '<button type="button" class="btn btn-primary">Save changes</button>';
    // html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    $('body').append(html).add( $('#messageAlert').modal('show'));
    setTimeout(function() {$('#messageAlert').modal('hide');}, 5000);
}
</script>


 <?php echo $this->app->script(); ?>