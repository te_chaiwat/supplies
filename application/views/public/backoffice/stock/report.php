<!-- Bootstrap DataTable -->
<link href="<?php echo base_url(); ?>assets/plugins/DataTable/datatables.min.css" rel="stylesheet"/>
<link href="<?php echo base_url(); ?>assets/plugins/DataTable/Buttons-1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet"/>
<div class="pb-5 no-padding" id="button-tolls">
</div>
<table cellspacing="0" class="display table table-bordered table-sm text-sm font-weight-light" id="tableData" style="font-size:13px;" width="100%">
	<thead class="bg-light">
		<th>
			<td>รหัส</td>
			<td>รายการ</td>
			<td>คงเหลือ</td>
			<td>ประเภท</td>
		</th>
	</thead>
	<tdoby>
		<?php foreach ($stock as $row): ?>
			<tr>
				<td>
					#
				</td>
				<td>
					<?php echo $row->supp_no; ?>
				</td>
				<td>
					<?php echo $row->supp_name; ?>
				</td>
				<td>
					<?php echo $row->supp_amount; ?>
				</td>
				<td>
					<?php echo $type = ($row->supp_type == 1) ? "วัสดุสำนักงาน" : "ครุภัณฑ์/อุปกรณ์สาขา"; ?>
				</td>
			</tr>
		<?php endforeach;?>
	</tdoby>
</table>
