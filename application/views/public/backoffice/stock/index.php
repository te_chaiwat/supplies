<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
<!--
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/lib/bootstrap-select/bootstrap-select.css">
<script src="<?php echo base_url(); ?>assets/js/lib/bootstrap-select/bootstrap-select.js"></script> -->
<!--=========================================
=            row header withdraw            =
==========================================-->
<div class="row-fluid mt-2 ml-2">
	<div class="col">
		<h1 class="page-header"> <i class="fa fa-list-alt"></i> รายการเบิก</h1>
		<hr>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!--====  End of row header withdraw  ====-->
<!--==========================
=            form            =
===========================-->
<section class="dashboard-counts section-padding">
	<!-- <div class="container-fluid"> -->
		<div class="row-fluid col-12">
			<form class="col-12" id="form">
				<div class="col-6 bg-light">
					<div class="form-row">
						<div class="col-md-2 mb-3">
							<label for="withdrawListNum">รหัสวัสดุ </label>
							<input type="text" class="form-control form-control-sm withdrawListNum" name="withdrawListNum[]" id="withdrawListNum1" placeholder="รหัสวัสดุ " >
						</div>
						<div class="col-md-7 mb-3">
							<label for="withdrawList">รายการ </label>
							<input type="text" class="form-control form-control-sm withdrawList" name="withdrawList[]" id="withdrawList1" placeholder="รายการ " >
						</div>
						<div class="col-md-2 mb-3 ">
							<label for="withdrawAmount">จำนวน</label>
							<input type="number" min="1" class="form-control form-control-sm withdrawAmount" name="withdrawAmount[]" id="withdrawAmount1" placeholder="จำนวน" >
						</div>
						<div class="col-md-1 mb-3 ">
							<label for="addwithdraw" class="d-flex "> &nbsp; </label>
							<button type="button" class="btn btn-primary float-right btn-sm" id="addwithdraw" name="addwithdraw"><i class="fa fa-plus"></i></button>
						</div>
						<div class="col-12 mb-3">
							<hr>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-8 mb-3">
							<label for="user_withdraw">ประเภท</label> <br>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="type1" value="1" name="withdrawType" class="custom-control-input" checked>
								<label class="custom-control-label" for="type1"> วัสดุสำนักงาน </label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="type2" value="2" name="withdrawType" class="custom-control-input">
								<label class="custom-control-label" for="type2"> ครุภัณฑ์/อุปกรณ์สาขา</label>
							</div>
						</div>
						<div class="col-md-4 mb-3">
							<label for="withdrawDate"> วัน/เดือน/ปี </label>
							<input type="text" name="withdrawDate" class="form-control form-control-sm text-center" id="withdrawDate" placeholder="วัน/เดือน/ปี" value="<?php echo date('d/m/y H:i:s'); ?>" readonly>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label for="withdrawDetail"> หมายเหตุ</label>
							<textarea class="form-control form-control-sm" rows="3" name="withdrawDetail" id="withdrawDetail" placeholder="หมายเหตุ" ></textarea>
						</div>
					</div>
				</div>
				<!-- end block left -->
				<div class="row-fluid ">
					<div class="col-6 bg-white list" style="height: 60vh;overflow-y: scroll;padding-right: 0px;">
						<table class="table table-striped table-sm w-100" border="1" id="showlist" >
							<caption id="tbheader" style="display: none;">
								<table style="width: 100vw; margin-left: 0px;" >
									<tr>
										<td class="col-sm-3 " style="widows: 100%;text-align: right;">
											<label > ต้นฉบับ</label>
											<br>
											<label style="width: 40%;padding-left: 0px;text-align: left;">เลขที่ </label>
											<label style="padding-right: 0%;text-align: right;">1234</label>
											<br>
											<label style="width: 40%;padding-left: 0px;text-align: left;">วันที่ </label>
											<label style="padding-right: 0%;text-align: right;"><?php echo date('d-m-Y'); ?></label>
											<br>
											<label style="width: 40%;padding-left: 0px;text-align: left;">ผู้ขอเบิก </label>
											<label style="padding-right: 0%;text-align: right;" id="repipient_user"></label>
											<br>
											<label style="width: 40%;padding-left: 0px;text-align: left;">ออกโดย </label>
											<label style="padding-right: 0%;text-align: right;"><?php echo $fullname; ?></label>
										</td>
									</tr>
								</table>
							</caption>
							<thead class="bg-light ">
								<tr>
									<th class="text-center"> รหัส </th>
									<th class="text-center"> รายการ </th>
									<th class="text-center"> จำนวน </th>
									<th class="text-center"> # </th>
								</tr>
							</thead>
							<tbody id ="tbody">
								<tr><td colspan="4" id='list1' class="text-center"> ========= NO DATA ========= </td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</form><!-- end form -->
			<div class="col-lg-12 text-center mt-3">
				<!-- <input type="checkbox" name="checkPrint" id="checkPrint" class="form-check-input" checked="check"> -->
				<label for="checkPrint"><i class="fa fa-print" aria-hidden="true"></i>ต้องการพิมพ์รายการ</label>
			</div>
			<!-- /.col-6 bg-light -->
			<div class="alert-warning col-12 text-center mt-3 pt-3 pb-3" style="bottom: 0px; position: relative;">
				<button class="btn btn-warning reset" type="reset">Reset form</button>
				<button class="btn btn-primary submit" type="submit">Submit form</button>
			</div>
		</div>
		<!-- </div> -->
	</section>
	<!--====  End of form  ====-->
	<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>