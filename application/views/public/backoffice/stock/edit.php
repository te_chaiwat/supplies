<!-- Bootstrap DataTable -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/DataTable/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/DataTable/Buttons-1.5.1/css/buttons.bootstrap4.min.css">

<div class="pb-5 no-padding" id="button-tolls"></div>
<table id="tableData" class="display table table-bordered table-sm text-sm font-weight-light" width="100%" cellspacing="0" style="font-size:13px;">
	<thead class="bg-light text-center font-weight-bold" style="font-size:16px;" >
		<th>
			<td>รหัส</td>
			<td>รายการ</td>
			<td>จำนวน</td>
			<td>ประเภท</td>
			<td>#</td>
		</th>
	</thead>
	<tdoby>
		<?php $i=1; ?>
		<?php foreach ($stock as $key => $row): ?>

			<tr id="tr<?php echo $row->supp_no; ?>" data-id="<?php echo $row->supp_id; ?>">
				<td>#</td>
				<td>
					<label class="d-none"><?php echo $row->supp_no; ?></label>
					<input type="text" name="supp_no" id="supp_no<?php echo $row->supp_no; ?>" class="form-control" value="<?php echo $row->supp_no; ?>">
				</td>
				<td>
					<label class="d-none"><?php echo $row->supp_name; ?></label>
					<input type="text" name="supp_name" id="supp_name<?php echo $row->supp_no; ?>" class="form-control" value="<?php echo $row->supp_name; ?>">
				</td>
				<td class="text-center">
					<input type="text" name="supp_amount" id="supp_amount<?php echo $row->supp_no; ?>" class="form-control" value="<?php echo $row->supp_amount; ?>">
				</td>
				<td>
					<div class="form-check">
						<input type="radio" value="1" name="withdrawType<?php echo $row->supp_no; ?>" id="supp_type<?php echo $key.$i; ?>" class="form-check-input" <?php echo $checked1 = ($row->supp_type == 1) ? "checked" : "ครุภัณฑ์/อุปกรณ์สาขา"; ?> />
						<label class="form-check-label" for="supp_type<?php echo $key.$i; ?>"> วัสดุสำนักงาน </label>
					</div>
					<div class="form-check">
						<input type="radio" value="2" name="withdrawType<?php echo $row->supp_no; ?>" id="supp_type<?php echo $key; ?>" class="form-check-input" <?php echo $checked2 = ($row->supp_type == 2) ? "checked" : "ครุภัณฑ์/อุปกรณ์สาขา"; ?> />
						<label class="form-check-label" for="supp_type<?php echo $key; ?>"> ครุภัณฑ์/อุปกรณ์สาขา</label>
					</div>
				</td>
				<td class=" text-center">
					<!-- <div class="form-group"> -->
						<button type="button" class="btn btn-danger btn-sm btn_delete" data-id="<?php echo $row->supp_no; ?>" title="ลบข้อมูล"><i class="fa fa-trash" aria-hidden="true"></i></button>
						<button type="button" class="btn btn-warning btn-sm btn_edit" data-id="<?php echo $row->supp_no; ?>" title="บันทึก"><i class="fa fa-save" aria-hidden="true"></i></button>
						<!-- </div> -->
					</td>
				</tr>

				<?php $i++; ?>
			<?php endforeach;?>
		</tbody>
	</table>


