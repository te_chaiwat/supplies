<!-- Bootstrap DataTable -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/DataTable/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/DataTable/Buttons-1.5.1/css/buttons.bootstrap4.min.css">

<div class="pb-5 no-padding" id="button-tolls"></div>
<table id="tableData" class="display table table-bordered table-sm text-sm font-weight-light" width="100%" cellspacing="0"
 style="font-size:13px;">
	<thead class="bg-light text-center font-weight-bold" style="font-size:16px;">
		<th>
		<td> รหัส </td>
		<td> ชื่อ - สกุล </td>
		<td> email </td>
		<td> เพศ </td>
		<td> สถานะ </td>
		<td> # </td>
		</th>
	</thead>
	<tbody>
		<?php
	foreach ($user as $key => $row) :
		if ($row->usergroupID != 1 && $row->usergroupID != 4) :
	?>
		<tr>
			<td></td>
			<td>
				<?php echo $row->user_code; ?>
			</td>
			<td>
				<?php echo $row->fullname; ?>
			</td>
			<td>
				<?php echo $row->email; ?>
			</td>
			<td class="text-center">
				<?php echo $gender = ($row->gender == 0) ? "ชาย" : "หญิง"; ?>
			</td>
			<td class="text-center">
				<?php $checked = ($row->usergroupID == 3) ? "checked='true'" : ""; ?>
				อาจารย์
				<label class="switch switch-3d switch-success mr-3">
					<input type="checkbox" class="switch-input" <?php echo $checked; ?>/>
					<span class="switch-label"></span>
					<span class="switch-handle"></span>
				</label>
				เจ้าหน้าที่
			</td>
			<td class="text-center">
			<button class="btn btn-danger"> <span class="fa fa-trash-o"></span> </button>
			</td>
		</tr>
		<?php
	endif;
	endforeach;
	?>
	</tbody>
</table>
