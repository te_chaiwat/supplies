<div class="content"  style="margin-top: 5px;padding: 20px 20px 20px 20px;">
	<h5>กำหนดสิทธิ์การใช้งาน </h5>
	<hr>
	<form action="<?php echo base_url().$this->layout.'submit'; ?>" method="post" accept-charset="utf-8">
		<div class="form-group">
			<label for="group" > เลือกกลุ่มผู้ใช้งาน</label>
			<select class="form-control" id="group" name="group" required oninvalid="this.setCustomValidity('กรุณาเลือกกลุ่มผุ้ใช้งาน')" oninput="setCustomValidity('')">
       <option value='' disabled selected> ---- เลือก ----- </option>
       option
       <?php foreach ($group as $keyGroup => $rowGroup): ?>
        <option value="<?php echo $rowGroup->usergroupID ;?>"><?php echo $rowGroup->usergroupName;?></option>
      <?php endforeach; ?>
    </select>
  </div>
  <div class="form-group ">
    <h6 class="font-weight-bold">จัดการเมนู</h6>
    <div class="col-12 manageMenu">

     <?php echo $menu; ?>
     <?php var_dump($menu); ?>

     <div class="col-12 text-center">
      <button class="btn btn-danger reset" type="button"><i class="fas fa-times" aria-hidden='true'></i> ยกเลิก</button>
      <button class="btn btn-primary" type="submit" id="submit"><i class="fas fa-save" aria-hidden='true'></i> บันทึก</button>
    </div>
    <!-- /.col-12 -->
  </div>
</div>

</form>
</div>
<!-- /.col-11 -->
