<div class="sufee-login d-flex align-content-center flex-wrap">
	<div class="container">
		<div class="login-content">
			<div class="login-logo">
				<a href="index.html">
					<img class="align-content" src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
				</a>
			</div>
			<div class="form-row col-sm-8 mx-auto">
				<?php if (!empty($_SESSION['message'])) : ?>
				<div class="alert alert-danger alert-dismissible fade show col-sm-12" role="alert">
					<?php echo $_SESSION['message']; ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?php endif; ?>
			</div>
			<div class="login-form">
				<form>
					<div class="form-group">
						<label>Email address</label>
						<input type="email" class="form-control" placeholder="Email" name="email">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" class="form-control" placeholder="Password" name="password">
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox"> Remember Me
						</label>
						<label class="pull-right">
							<a href="#">Forgotten Password?</a>
						</label>

					</div>
					<button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
					<div class="register-link m-t-15 text-center">
						<p>Don't have account ? <a href="<?php echo base_url('authen/regis'); ?>"> Sign Up Here</a></p>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
