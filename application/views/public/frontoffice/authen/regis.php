<div class="row">
	<div class="col-12">
		<div class="login-logo ">
			<a href="index.html">
				<img class="align-content" src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
			</a>
		</div>
		<hr>
	</div>
	<div class="form-row col-sm-8 mx-auto">
		<?php if (!empty($_SESSION['message'])) : ?>
		<div class="alert alert-danger alert-dismissible fade show col-sm-12" role="alert">
			<?php echo $_SESSION['message']; ?>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<?php endif; ?>
	</div>
	<div class="col-8 mx-auto bg-white">
		<form class="mb-3 mt-3" id="formregis">
			<div class="form-row">
				<div class="form-group col-6">
					<label for="inputEmail4">Email</label>
					<input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email" required />
				</div>
				<div class="form-group col-6">
					<label for="inputPassword4">Password</label>
					<input type="password" name="password" class="form-control" id="inputPassword4" placeholder="Password" required />
				</div>
			</div>
			<div class="form-group">
				<label for="inputFullName"> ชื่อ - สกุล</label>
				<input type="text" name="fullname" class="form-control" id="inputFullName" required />
			</div>
			<div class="form-row">
				<div class="form-group col-sm-4">
					<label class="form-label"> เพศ </label>
					<div class="form-group col-12 mt-2">
						<h4>
							<div class="form-check col-6">
								<input type="radio" value="1" name="gender" id="sex1" class="form-check-input" style="transform: scale(1.5);"
								 checked="check" />
								<label class="form-check-label" for="sex1"> ชาย </label>
							</div>
							<div class="form-check col-6">
								<input type="radio" value="2" name="gender" id="sex2" class="form-check-input " style="transform: scale(1.5);" />
								<label class="form-check-label" for="sex2"> หญิง</label>
							</div>
						</h4>
					</div>
				</div>
				<div class="form-group col-sm-3">
					<label for="userCode">รหัสประจำตัว</label>
					<input type="text" class="form-control" name="user_code" id="userCode">
				</div>
				<div class="form-group col-sm-5">
					<label for="department">แผนก/สาขา</label>
					<select id="department" name="department" class="form-control" required />
					<option selected disabled> ... เลือก แผนก/สาขา...</option>
					<?php foreach ($department as $key => $row) : ?>
					<option value="<?php echo $row->dep_id; ?>">
						<?php echo $row->dep_name; ?>
					</option>
					<?php endforeach ?>
					</select>
				</div>
			</div>
			<input type="hidden" name="status" value="ON" />
			<input type="hidden" name="group" value="2" />
			<div class="form-row col-12">
				<div class="ml-auto">
					<button type="submit" class="btn btn-success">Sign in</button>
					<a href="<?php echo base_url(); ?>" class="btn btn-warning">Exit</a>
				</div>
			</div>
		</form>
		<div class="register-link m-t-15 text-center">
			<p>Already have account ? <a href="<?php echo base_url('authen'); ?>"> Sign in</a></p>
		</div>
	</div>
</div>
