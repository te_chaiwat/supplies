<style type="text/css">

page {
	background: white;
	display: block;
	margin: 0 auto;
	padding-left: 1cm;
	margin-bottom: 0.5cm;
	box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {
	padding-left: 1cm;
	width: 29cm;
	height: 42cm;
}
page[size="A4"][layout="portrait"] {
	width: 42cm;
	height: 29cm;
}

page[size="sticker"]{
	padding-left: 1cm;
	widows: 17.5cm;
	height: 22.5cm;
}

@media print {
	body, page {
		padding-left: 1cm;
		margin: 0;
		box-shadow: 0;
		-webkit-print-color-adjust: exact !important; /*Chrome, Safari */
		color-adjust: exact !important;  /*Firefox*/
	}
}


</style>
<div class="col-12 mb-4">
	<button id="print"><i class="fa fa-print"></i> Prfint</button>
</div>
<div class="col-12">
	<page id="paper" size="sticker">
		<?php for($i=0; $i <= 5; $i++): ?>
			<img id='barcode1' src='http://bwipjs-api.metafloor.com/?bcid=code128&text=123<?php echo $i;?>' style='width:1.8cm;padding-left:0.12cm;height:1cm;margin-top:0px;'>
			<br><br><br>
		<?php endfor ?>
		<!-- <img id='barcode2' src='http://bwipjs-api.metafloor.com/?bcid=code128&text=123' style='width:1.8cm;margin-left:0.2cm;height:1.4cm;margin-top:0px;'>
		<img id='barcode3' src='http://bwipjs-api.metafloor.com/?bcid=code128&text=123' style='width:1.8cm;margin-left:0.2cm;height:1.4cm;margin-top:0px;'>
		<img id='barcode4' src='http://bwipjs-api.metafloor.com/?bcid=code128&text=123' style='width:1.8cm;margin-left:0.2cm;height:1.4cm;margin-top:0px;'>
		<img id='barcode5' src='http://bwipjs-api.metafloor.com/?bcid=code128&text=123' style='width:1.8cm;margin-left:0.2cm;height:1.4cm;margin-top:0px;'> -->
	</page>
</div>
<script>

	$('#print').click(function(){

		w =window.open('','name','height=842,width=1024,scrollbars=yes,status=yes');
		w.document.write($('#paper').html() );
		w.print();

		w.close()
	});
</script>