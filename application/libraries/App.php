<?php
defined('BASEPATH') or exit('No direct script access allowed');

class App
{
    protected $ci;
    private $layout;
    private $title;
    private $active;

    public function __construct()
    {
        $this->ci = &get_instance();
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function setLayout($var)
    {
        $this->layout = $var;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($var)
    {
        $this->title = $var;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($var)
    {
        $this->active = $var;
    }

    public function render($title = "Supplies CS & IT", $render, $data = null, $backoffice = true)
    {
        $this->setTitle($title);
        $view = $this->ci->load->view('public/' . $render, $data, true);
        $this->setLayout($view);
        if ($backoffice) {
            // $this->ci->load->view('templates/backoffice/content', null);
            $this->ci->load->view('templates/frontoffice/content');
        } else {
            $this->ci->load->view('templates/frontoffice/content', null);
        }
    }

    public function script()
    {
        $current = $this->ci->uri->segment(1);
        $script  = $this->ci->uri->segment(2);
        if (file_exists('assets/scripts/' .$current.'/'. $script.'.js' )) {
            return "<script src='" . base_url() . "assets/scripts/" . $current . "/" . $script . ".js'></script>";
        } elseif (file_exists('assets/scripts/' . $current .'/'. $current.'.js')) {
            return "<script src='" . base_url() . "assets/scripts/" . $current . "/" . $current . ".js'></script>";
        } elseif (file_exists('assets/scripts/' . $this->ci->uri->segment(0) )) {
            return "<script src='" . base_url() . "assets/scripts/welcome.js'></script>";
        } else {
            // return false;
            return "";
        }
    }
}

/* End of file App.php */
/* Location: ./application/libraries/App.php */
