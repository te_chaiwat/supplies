<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('account_model');
        $this->layout = 'backoffice/account/';

         if (!empty($this->session->userdata('userID'))) {
            $this->dataLogin = $this->session->userdata();
        } else {
            redirect('index.php/authen', 'refresh');
        }

    }

    public function index()
    {
        $this->app->render('Manage Account', $this->layout . 'index', null, null);
    }

    public function management()
    {
        $this->data['user'] = $this->account_model->getAccountAll();
        $this->app->render('Management User', $this->layout.'manageUser', $this->data, true);
    }

}

/* End of file Account.php */
/* Location: ./application/controllers/Account.php */
