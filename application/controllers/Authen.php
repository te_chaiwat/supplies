<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Authen extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('crud_model');
        $this->load->model('authen_model');
        $this->load->model('authen_model');
        $this->table = 'account';
        $this->load->database();
        $this->data = $this->session->userdata();

        $this->layout = 'frontoffice/authen/';
    }

    public function index()
    {
        $this->data = '';
        $this->app->render('Authen Withdraw', $this->layout . 'login', $this->data, null);
    }

    public function regis()
    {
        $this->data['department'] = $this->authen_model->getDepartmentAll();
        $this->app->render('Authen Regis', $this->layout . 'regis', $this->data, null);
    }

    public function addRegis()
    {

        if ($_POST) {
            $data = array(
                'email'       => $this->input->post('email'),
                'password'    => md5($this->input->post('password')),
                'fullname'    => $this->input->post('fullname'),
                'gender'      => $this->input->post('gender'),
                'user_code'   => $this->input->post('user_code'),
                'dep_id'      => $this->input->post('department'),
                'status'      => $this->input->post('status'),
                'usergroupID' => $this->input->post('group'),
            );
            $userRS = $this->authen_model->CheckValidLogin($data['email'], trim($data['password']));
            if ($userRS > 0) {
                //check data account num row > 0
                $this->data['loginfail'] = $this->session->set_flashdata('message', ' USER E-MAIL นี้มีคนใช้งานแล้ว !!');
                $this->app->render("Supplies Sign IN", $this->layout . 'regis', $this->data, null);
            } else {
                $add = $this->crud_model->Insert($this->table, $data);

                if (!empty($add)) {
                    $authenRS = $this->authen_model->CheckValidLogin($data['email'], trim($data['password']));
                    if (count($authenRS) > 0) {
                        $this->setSession($authenRS);
                    } else {
                        $this->data['loginfail'] = $this->session->set_flashdata('message', ' USER E-MAIL OR PASSWORD FAIL !!');
                        $this->app->render("Supplies Sign IN", $this->layout . 'regis', $this->data, null);
                    }
                } else {
                    redirect('authen/regis', 'refresh');
                }
            }
        }
    }

    public function checkLogin()
    {
        if ($_POST) {
            $authenRS = $this->authen_model->CheckValidLogin($this->input->post('email'), trim(md5($this->input->post('password'))));
            if (!empty($authenRS)) {
                $this->setSession($authenRS);
            } else {
                $this->data['loginfail'] = $this->session->set_flashdata('message', ' USER E-MAIL OR PASSWORD FAIL !!');
                $this->app->render("Supplies Sign IN", $this->layout . 'login', $this->data, null);
            }
        }
        else {
            redirect('dashboard', 'refresh');
        }
    }

    public function setSession($authenRS)
    {
        $this->loginSession = array(
            'userID'        => $authenRS[0]['id'],
            "email"         => $authenRS[0]['email'],
            "fullname"      => $authenRS[0]['fullname'],
            'userGroup'     => $authenRS[0]['usergroupID'],
            'usergroupName' => $authenRS[0]['usergroupName'],
            'dep_id'        => $authenRS[0]['dep_id'],
            'dep_name'      => $authenRS[0]['dep_name'],
        );
        $this->session->set_userdata($this->loginSession);
// เสร็จแล้วไปที่หน้า dashboard
        redirect('dashboards', 'refresh');
    }

    public function logOut()
    {
        $userID   = $this->session->unset_userdata("userID");
        $email    = $this->session->unset_userdata("email");
        $fullname = $this->session->unset_userdata("fullname");
        session_destroy();
        redirect('authen/', 'refresh');
    }
}

/* End of file Authen.php */
/* Location: ./application/controllers/Authen.php */
