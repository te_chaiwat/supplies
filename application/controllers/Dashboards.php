<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboards extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->layout = 'frontoffice/';

    if($this->session->userdata()){
			$this->dataLogin = $this->session->userdata();
		}else{
			$this->dataLogin = null;
		}
  }

  public function index()
  {
    $this->app->render('Supplies CS&IT',$this->layout.'index',null,null);
  }

}

/* End of file Deshboards.php */
/* Location: ./application/controllers/Deshboards.php */