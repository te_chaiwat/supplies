<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stock extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('stock_model', '', true);
        $this->load->model('crud_model');
        $this->table = 'supplies_stock';
        $this->load->database();
        $this->layout = 'backoffice/stock/';
        if (!empty($this->session->userdata('userID'))) {
            $this->dataLogin = $this->session->userdata();
        } else {
            redirect('index.php/authen', 'refresh');
        }
    }

    public function index()
    {
        echo "blank";
    }

    public function addStock()
    {
        $this->app->render('Withdraw', $this->layout . 'index', null, true);
    }

    public function getStockAll()
    {
        $stockAll = $this->stock_model->getStockAll();

        echo json_encode($stockAll);
    }

    public function getStockWhereNo()
    {
        $stockWhere = $this->stock_model->getStockWhereNo($this->input->post('supp_no'));
        echo json_encode($stockWhere);
    }

    public function getStockWhereName()
    {
        $stockWhere = $this->stock_model->getStockWhereName($this->input->post('supp_name'));
        echo json_encode($stockWhere);
    }

    public function report()
    {
        $this->data['stock'] = $this->stock_model->getStockAll();
        $this->app->render('STOCK REPORT', $this->layout . 'report', $this->data, true);
    }

    public function edit()
    {
        $this->data['stock'] = $this->stock_model->getStockAll();
        $this->app->render('Edit Stock', $this->layout . 'edit', $this->data, true);
    }

    public function editData()
    {
        $supp_id                   = $this->input->post('supp_id');
        $this->data['supp_no']     = $this->input->post('supp_no');
        $this->data['supp_name']   = $this->input->post('supp_name');
        $this->data['supp_amount'] = $this->input->post('supp_amount');
        $this->data['supp_type']   = $this->input->post('supp_type');
        $this->crud_model->update($this->table, "supp_id", $supp_id, $this->data);

        echo json_encode("ok");
    }

    public function deleteStock()
    {
        $idStock = $this->input->post('stockID');
        $this->crud_model->delete($this->table, 'supp_no', $idStock);
        echo json_encode($idStock);
    }

    public function submit()
    {

        $stockAll    = $this->stock_model->getStockAll();
        $count       = count($this->input->post('withdrawList'));
        $num         = ($this->input->post('withdrawList')[0] == '') ? $num         = 1 : $num         = 0;
        $insertStock = array();
        $updateStock = array();
        $addStock    = array();
        $dataInsert  = array();

        //set data update
        for ($i = $num; $i < $count; $i++) {
            $dataList[$i] = array(
                'supp_no'     => $this->input->post('withdrawListNum')[$i],
                'supp_name'   => $this->input->post('withdrawList')[$i],
                'supp_amount' => $this->input->post('withdrawAmount')[$i],
                'supp_type'   => $this->input->post('withdrawType'),
                'supp_date'   => $this->input->post('withdrawDate'),
                'supp_detail' => $this->input->post('withdrawDetail'),
            );
            array_push($dataInsert, $dataList[$i]);
            foreach ($stockAll as $row => $value) {
                if ($value->supp_no == $dataList[$i]['supp_no']) {
                    //set data update
                    array_push($updateStock, $dataList[$i]);

                }

            }
        }

        foreach ($updateStock as $value) {
            // send data to update Stodk
            $this->stock_model->addStockUpdate($value);

            foreach ($dataInsert as $key => $valInsert) {
                if ($value == $valInsert) {
                    // set data Insert stock
                    unset($dataInsert[$key]);
                    break;
                }
            }
        }
        foreach ($dataInsert as $dataInsertList) {
            // send data Insert stock
            $this->crud_model->Insert($this->table, $dataInsertList, $id = null);
        }
    }

}

/* End of file Stock.php */
/* Location: ./application/controllers/Stock.php */
