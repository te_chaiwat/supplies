<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->layout = 'index.php/welcome/';

		if($this->session->userdata()){
			$this->dataLogin = $this->session->userdata();
		}else{
			$this->dataLogin = null;
		}
	}

	public function index()
	{
		$this->app->render('Supplies CS&IT','frontoffice/index', null, null);
	}

	public function genBarcode()
	{
		$this->app->render('GENERATE BARCODE','frontoffice/gen_barcode',null,null);
	}
}
