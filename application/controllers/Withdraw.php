<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdraw extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('withdraw_model','',TRUE);
		$this->load->model('stock_model','',TRUE);
		$this->load->model('crud_model');
		$this->table = 'list_withdraw';
		$this->layout = 'frontoffice/withdraw/';

		$this->load->database();

		if (!empty($this->session->userdata('userID'))) {
			$this->dataLogin = $this->session->userdata();
		} else {
			redirect('index.php/authen', 'refresh');
		}
	}

	public function index()
	{
		$this->data['show_user'] = $this->withdraw_model->get_user();
		$this->data['show_stock'] = $this->stock_model->getStockAll();

		$this->app->render('Withdraw', $this->layout.'index', $this->data, null);
	}

	public function submit()
	{
		$count = count($this->input->post('withdrawList'));
		$num = ($this->input->post('withdrawList')[0] == '')? $num = 1 : $num = 0;

		for($i = $num; $i < $count; $i++){
			$dataList[$i] = array(
				'list_no' => $this->input->post('withdrawListNum')[$i],
				'list_name' => $this->input->post('withdrawList')[$i],
				'list_amount' => $this->input->post('withdrawAmount')[$i],
				'user_id' => $this->input->post('user_withdraw'),
				'list_date' => $this->input->post('withdrawDate'),
				'list_detail' => $this->input->post('withdrawDetail')
			);
			$this->stock_model->cutStock($dataList[$i]);
			$this->crud_model->Insert($this->table,$dataList[$i],$id=null);
		}
		// echo json_encode("OK");
	}

}
/* End of file Withdraw.php */
/* Location: ./application/controllers/Withdraw.php */
