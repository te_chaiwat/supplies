<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usersetting extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('account_model');
		$this->load->model('crud_model');
		$this->load->model('menu_model');
		$this->table = 'menu_config';
		$this->load->database();
		$this->layout = 'backoffice/usersetting/';
		if (!empty($this->session->userdata('userID'))) {
			$this->dataLogin = $this->session->userdata();
		} else {
			redirect('index.php/authen', 'refresh');
		}
	}

	public function index()
	{
		$this->data['group'] = $this->getGroupAll();
		$this->data['menu']  = $this->getMenuAll();
		$this->app->render('กำหนดสิทธิ์การใช้งาน', $this->layout . 'index', $this->data, null);
	}

	public function getGroupAll()
	{
		return $this->account_model->getUsergroupAll();
	}

	public function getDataSelect()
	{
		if($_POST){
			$post = $this->input->post();
			$dataConfigMenu = $this->menu_model->showMenuON($post['idSelect']);
			echo json_encode($dataConfigMenu);
		}
	}

	public function getMenuAll()
	{
		$menuLevel1 = $this->menu_model->setmenu('1', 4);
		$menuLevel2 = $this->menu_model->setmenu('2', 4);
		$menu       = "";
		foreach ($menuLevel1['result'] as $mlevel1) {
            // $active =  $this->uri->segment(1) == $mlevel1->menu_url ? 'active': '';
            // $icon =  $mlevel1->menu_icon != ''? $mlevel1->menu_icon : '';

			if ($mlevel1->menu_url !== '#') {
				$menu .= '<li>';
				$menu .= '<label><input type="checkbox" name="menuID[]" id="menu' . $mlevel1->menuID . '" value="' . $mlevel1->menuID . '"> ' . $mlevel1->menu_name . '</label>';
				$menu .= '</li>';
			} else {
				$menu .= '<li>';
				$menu .= '<label><input type="checkbox" name="menuID[]" id="menu' . $mlevel1->menuID . '" value="' . $mlevel1->menuID . '"> ' . $mlevel1->menu_name . '</label>';

				foreach ($menuLevel2['result'] as $mlevel2) {
					if ($mlevel1->menuID == $mlevel2->menu_parent) {
						$menu .= '<li class="ml-5">';
						$menu .= '<label><input type="checkbox" name="menuID[]" id="menu' . $mlevel2->menuID . '" value="' . $mlevel2->menuID . '"> ' . $mlevel2->menu_name . '</label>';
						$menu .= "</li>";
					}
					$menu .= ' </li>';
				}
//------- end foreach ----------//
			}
            //---------- end else ----------//
		}
		return $menu;
	}

}

/* End of file Usersetting.php */
/* Location: ./application/controllers/Usersetting.php */
