<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function getMenulAll()
	{
		$this->db->select('menuID,menu_name,menu_parent');
		return $this->db->get('menu')->result();
	}

	public function setMenu($level,$group)
	{
		$sql = "
		SELECT *
		FROM  menu
		INNER JOIN menu_config mc ON mc.menuID = menu.menuID
		INNER JOIN user_group ug ON ug.usergroupID = mc.userGroupID
		WHERE menu.menu_level ='$level'
		";
		$sql .= ($group != null) ? "AND mc.userGroupID = '$group' " : "";
		$sql .="
		AND menu.menu_status = '1'
		AND mc.status= 'ON'
		ORDER BY menu.menuID
		";
		$query = $this->db->query($sql);
		$num_rows = $query->num_rows();
		$result = $query->result();
		$data = array(
			'num_rows' => $num_rows,
			'result' => $result
		);
		return $data;
	}

	public function showMenuON($groupID)
	{
		$sql = "
		SELECT *
		FROM menu
		INNER JOIN menu_config mc ON mc.menuID = menu.menuID
		INNER JOIN user_group ug ON ug.usergroupID = mc.userGroupID
		";
		if(isset($groupID)){
			$sql .= "WHERE ug.userGroupID = '$groupID' ";
		}
		$sql .= " AND mc.status = 'ON' ";
		return $this->db->query($sql)->result_array();
	}
}

/* End of file Menu_model.php */
/* Location: ./application/models/Menu_model.php */