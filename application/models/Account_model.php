<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account_model extends CI_Model
{

	public function getAccountAll()
	{
		$data = $this->db->get('account');
		return $data->result();
	}

	public function getUsergroupAll()
	{
		$this->db->select('usergroupID,usergroupName,status');
		$this->db->where('userGroupID <> 4');
		return $this->db->get('user_group')->result();
	}

}
