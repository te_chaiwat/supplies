<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stock_model extends CI_Model
{

    public function getStockAll()
    {
        $query = $this->db->get('supplies_stock');
        return $query->result();
    }

    public function getStockWhereNo($suppNo = "")
    {
        $query = $this->db->get_where('supplies_stock', array('supp_no' => $suppNo));
        return $query->result();
    }

    public function getStockWhereName($suppName = "")
    {
        $query = $this->db->get_where('supplies_stock', array('supp_name' => $suppName));
        return $query->result();
    }

    public function cutStock($data = '')
    {
        $query = $this->db->query("UPDATE supplies_stock SET supp_amount = supp_amount - " . $data['list_amount'] . " WHERE supp_no = " . $data['list_no']);
        return true;
    }

    // public function insertStock($data)
    // {
    //     echo "insertStock <pre>";
    //     print_r($data);
    // }

    public function addStockUpdate($data)
    {
        $datastock = $this->getStockWhereNo($data['supp_no']);
        foreach ($datastock as $key => $rowStock) {
            $amount = $rowStock->supp_amount + $data['supp_amount'];
            $dataUpdate = array(
                'supp_amount' => $amount,
                'dt_create'   => date('Y-m-d H:i:s'),
                'created_by'  => $this->session->userdata('userID'),
            );
            $this->db->where('supp_no', $data['supp_no']);
            $this->db->update('supplies_stock', $dataUpdate);
        }
        return true;
    }

}

/* End of file Stock_model.php */
/* Location: ./application/models/Stock_model.php */
