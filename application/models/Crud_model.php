<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Crud_model extends CI_Model
{

	public function Insert($table, $data, $id = null)
	{
// $user_id = '1'; //test insert data by user_id
		if (!empty($data)) {
			$data['dt_create']  = date('Y-m-d H:i:s');
			$data['created_by'] = $this->session->userdata('userID');
			$this->db->insert($table, $this->security->xss_clean($data));
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function update($table, $field, $id, $data)
	{
		if(!empty($id)){
			$data['dt_create']  = date('Y-m-d H:i:s');
			$data['created_by'] = $this->session->userdata('userID');
			$this->db->where($field, $id);
			$this->db->update($table, $data);
			return $id;
		}else{
			return false;
		}
	}

	public function delete($table, $field, $id = null)
	{
		if (!empty($id)) {
			$this->db->where($field, $id);
			$this->db->delete($table);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Crud_model.php */
/* Location: ./application/models/Crud_model.php */
