-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 11, 2018 at 11:58 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `supplies_csit`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL COMMENT 'ID Account',
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email Account',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('0','1') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=male,1=female',
  `user_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'รหัส Account',
  `dep_id` int(11) NOT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci NOT NULL,
  `usergroupID` int(11) NOT NULL COMMENT 'ประเภท Account',
  `dt_create` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `email`, `password`, `fullname`, `gender`, `user_code`, `dep_id`, `status`, `usergroupID`, `dt_create`, `created_by`) VALUES
(12, 'mynameistecs51@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'อาจารย์', '0', '123456789', 2, 'ON', 2, '2018-01-15 04:55:16', 1),
(13, 'te.chaiwat@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'หอมแสง ไชยวัฒน์', '0', '0', 2, 'ON', 4, '2018-01-17 09:16:54', 1),
(25, 'chaiwat@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'นักศึกษา', '0', '0', 1, 'ON', 3, '2018-01-29 08:30:47', 13),
(29, 'a@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'admin', '0', '12', 1, 'ON', 1, '2018-03-14 01:17:15', 29),
(30, 'te@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'chaiwat หอมแสง', '0', '2223333', 1, 'ON', 2, '2018-03-14 01:17:17', 29),
(31, 'homsang@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'homsang', '1', '333333', 1, 'ON', 2, '2018-03-13 17:46:03', 29);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id_course` int(11) NOT NULL,
  `course_id` varchar(10) NOT NULL,
  `course_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id_course`, `course_id`, `course_name`) VALUES
(1, 'GE10001', 'ภาษาไทยเพื่อการสื่อสาร'),
(2, 'GE10004', 'สุนทรียภาพในภาษาไทย '),
(3, 'GE10002', 'ภาษาอังกฤษเพื่อการสื่อสาร'),
(4, 'GE10005', 'ภาษาอังกฤษเพื่อการสื่อสารในสถานการณ์เฉพาะหน้า'),
(5, 'GE10003', 'การอ่านและการเขียนภาษาอังกฤษเพื่อจุดประสงค์ทั่วไป'),
(6, 'GE10006', 'การอ่านและการเขียนภาษาอังกฤษเพื่อการนําไปใช้'),
(7, 'GE20001', 'จริยธรรมเพื่อการดํารงชีวิต'),
(9, 'GE20005', 'ศาสนาเพื่อการดํารงชีวิต'),
(10, 'GE20002', 'สุนทรียภาพเพื่อชีวิต'),
(11, 'GE20006', 'สุนทรียภาพในอีสาน'),
(12, 'GE20003', 'พฤติกรรมมนุษย์เพื่อการพัฒนาตน');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dep_id` int(11) NOT NULL,
  `dep_name` text NOT NULL,
  `dt_create` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dep_id`, `dep_name`, `dt_create`, `created_by`) VALUES
(1, 'วิทยาการคอมพิวเตอร์', '0000-00-00 00:00:00', 0),
(2, 'เทคโนโลยีสารสนเทศ', '0000-00-00 00:00:00', 0),
(3, 'วิทยาศาสตร์สิ่งแวดล้อม', '2018-09-21 10:56:03', 0),
(4, 'คณิตศาสตร์', '2018-09-21 10:58:38', 0),
(5, 'สถิติประยุกต์', '2018-09-21 10:58:38', 0),
(6, 'วิทยาศาสตร์การกีฬา', '2018-09-21 10:58:38', 0),
(7, 'วิทยาศาสตร์สุขภาพ', '2018-09-21 10:58:38', 0),
(8, 'ชีววิทยา', '2018-09-21 10:58:38', 0),
(9, 'เคมี', '2018-09-21 10:58:38', 0),
(10, 'ฟิสิกส์', '2018-09-21 10:58:38', 0);

-- --------------------------------------------------------

--
-- Table structure for table `list_withdraw`
--

CREATE TABLE `list_withdraw` (
  `list_id` int(11) NOT NULL,
  `list_no` varchar(5) NOT NULL,
  `list_name` text NOT NULL,
  `list_amount` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `list_date` text NOT NULL,
  `list_detail` text NOT NULL,
  `dt_create` datetime NOT NULL,
  `created_by` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `list_withdraw`
--

INSERT INTO `list_withdraw` (`list_id`, `list_no`, `list_name`, `list_amount`, `user_id`, `list_date`, `list_detail`, `dt_create`, `created_by`) VALUES
(1, '567', 'ดินสอ', 5, '12', '10/10/18 16:30:00', '', '2018-10-10 16:30:28', '30'),
(2, '890', 'ยางลบ', 2, '12', '10/10/18 16:30:00', '', '2018-10-10 16:30:28', '30'),
(3, '567', 'ดินสอ', 1, '2', '10/10/18 16:31:25', '', '2018-10-10 16:31:53', '30'),
(4, '890', 'ยางลบ', 1, '2', '10/10/18 16:31:25', '', '2018-10-10 16:31:53', '30'),
(5, '567', 'ดินสอ', 1, '13', '10/10/18 16:32:25', '', '2018-10-10 16:32:46', '30'),
(6, '890', 'ยางลบ', 1, '13', '10/10/18 16:32:25', '', '2018-10-10 16:32:46', '30');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menuID` int(11) NOT NULL,
  `menu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ชื่อเมนู',
  `menu_parent` int(11) DEFAULT NULL COMMENT 'id รายการแม่ อ้างอิงว่า แถวนี้อยุ่ใต้ id ไหน',
  `menu_order` int(11) DEFAULT NULL COMMENT 'ลำดับLevel',
  `menu_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'controller',
  `menu_level` int(11) DEFAULT NULL COMMENT 'ลำดับlevel',
  `menu_status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT 'สถานะรายงาน',
  `dt_create` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menuID`, `menu_name`, `menu_parent`, `menu_order`, `menu_icon`, `menu_url`, `menu_level`, `menu_status`, `dt_create`, `created_by`) VALUES
(1, 'ทั่วไป', 0, 1, '<i class=\"fa fa-home\" aria-hidden=\"true\"></i>', 'index.php/welcome', 1, '1', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_config`
--

CREATE TABLE `menu_config` (
  `menuConfigID` int(11) NOT NULL,
  `userGroupID` int(11) DEFAULT NULL,
  `menuID` int(11) DEFAULT NULL,
  `canAdd` int(30) DEFAULT NULL,
  `canEdit` int(30) DEFAULT NULL,
  `canView` int(30) DEFAULT NULL,
  `canDrop` int(30) DEFAULT NULL,
  `canApprove` int(30) DEFAULT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_config`
--

INSERT INTO `menu_config` (`menuConfigID`, `userGroupID`, `menuID`, `canAdd`, `canEdit`, `canView`, `canDrop`, `canApprove`, `status`, `dt_create`, `created_by`) VALUES
(1, 4, 4, 1, 1, 1, 1, 1, 'ON', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplies_stock`
--

CREATE TABLE `supplies_stock` (
  `supp_id` int(11) NOT NULL,
  `supp_no` varchar(11) NOT NULL,
  `supp_name` text NOT NULL,
  `supp_amount` int(11) NOT NULL,
  `supp_type` enum('1','2') NOT NULL COMMENT '1=วัสดุสำนักงาน,2=ครุภัณฑ์/อุปกรณ์สาขา ',
  `supp_date` varchar(16) NOT NULL,
  `dt_create` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplies_stock`
--

INSERT INTO `supplies_stock` (`supp_id`, `supp_no`, `supp_name`, `supp_amount`, `supp_type`, `supp_date`, `dt_create`, `created_by`) VALUES
(2, '567', 'ดินสอ', 19, '2', '14/07/2015', '2018-09-20 14:11:41', 30),
(3, '890', 'ยางลบ', 21, '2', '15/07/2015 11:36', '2018-10-08 08:49:02', 30);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `usergroupID` int(11) NOT NULL,
  `usergroupName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dt_create` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`usergroupID`, `usergroupName`, `status`, `dt_create`, `created_by`) VALUES
(1, 'ADMIN', 'ON', '2018-01-12 00:00:00', 1),
(2, 'TEACHER', 'ON', '2018-01-12 00:00:00', 1),
(3, 'SUPPORT STAFF', 'ON', '0000-00-00 00:00:00', 1),
(4, 'SUPER USER', 'ON', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_supplies`
--

CREATE TABLE `user_supplies` (
  `user_id` int(11) NOT NULL,
  `user_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_supplies`
--

INSERT INTO `user_supplies` (`user_id`, `user_name`) VALUES
(1, 'สุทธิกานต์ บ่จักรพันธ์'),
(2, 'นิพล สังสุทธิ'),
(3, 'ไพศาล ดาแร่'),
(4, 'อารีรัตน์ วุฒิเสน'),
(5, 'นิพล สังสุทธิ'),
(6, 'วันทนี รัฐสมุทร'),
(7, 'ปิยวัจน์ ค้าสบาย'),
(8, 'ปัจจัย พวงสุวรรณ'),
(9, 'ปณวรรต คงธนกุลบวร'),
(10, 'ภาณุพันธุ์ ชื่นบุญ'),
(11, 'ตรีรัตน์ เสริมทรัพย์'),
(12, 'คุณาวุฒิ บุญกว้าง'),
(13, 'คณิศร จี้กระโทก'),
(14, 'ขวัญชัย สุขแสน'),
(15, 'กริช สมกันทา'),
(16, 'ปิยสุดา ตัณเลิศ'),
(17, 'เรวดี พิพัฒน์สูงเนิน'),
(18, 'พิศณุ ชัยจิตวณิกุล'),
(19, 'พนารัตน์ ศรีเชษฐา'),
(20, 'มานะ โสภา'),
(21, 'ณรรฐวรรณ์ พูลสน');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id_course`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dep_id`);

--
-- Indexes for table `list_withdraw`
--
ALTER TABLE `list_withdraw`
  ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menuID`);

--
-- Indexes for table `menu_config`
--
ALTER TABLE `menu_config`
  ADD PRIMARY KEY (`menuConfigID`);

--
-- Indexes for table `supplies_stock`
--
ALTER TABLE `supplies_stock`
  ADD PRIMARY KEY (`supp_id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`usergroupID`);

--
-- Indexes for table `user_supplies`
--
ALTER TABLE `user_supplies`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Account', AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id_course` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `list_withdraw`
--
ALTER TABLE `list_withdraw`
  MODIFY `list_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menuID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_config`
--
ALTER TABLE `menu_config`
  MODIFY `menuConfigID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `supplies_stock`
--
ALTER TABLE `supplies_stock`
  MODIFY `supp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `usergroupID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_supplies`
--
ALTER TABLE `user_supplies`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
