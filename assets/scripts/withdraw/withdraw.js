var $ = jQuery;
$(document).ready(function() {
    firstList();
    addList();
    // include(url+'assets/js/lib/chosen/chosen.jquery.min.js');
    // include(url+'assets/selectpicker/bootstrap-select.min.js');
    // $("form").attr({"action": url+ctl+"/submit", "method": "post"});
    autocomplete();
    submitReset();
});

function firstList() {
    $('#withdrawListNum1').focus();
    $('#withdrawListNum1').keydown(function(e) { //checkwithdrawListnum on key down
        if (e.keyCode == 13) {
            $.ajax({
                url: url + 'stock/getStockWhereNo/',
                type: 'POST',
                dataType: 'json',
                data: {
                    supp_no: $(this).val()
                },
                success: function(res) {
                    $.each(res, function(i, f) {
                        $('#withdrawList1').val(f.supp_name);
                        if ($('#withdrawAmount1').val() >= 1) {
                            $('#withdrawAmount1').val();
                            $('#addwithdraw').trigger('click');
                        } else {
                            $('#withdrawAmount1').val('1');
                            $('#addwithdraw').trigger('click');
                        }
                    });
                },
                error: function() {
                    messageAlert("มีบางอย่างผิดพลาด !!");
                }
            });
        } else if (e.keyCode == 9) {
            $.ajax({
                url: url + 'stock/getStockWhereNo/',
                type: 'POST',
                dataType: 'json',
                data: {
                    supp_no: $(this).val()
                },
                success: function(res) {
                    $.each(res, function(i, f) {
                        $('#withdrawList1').val(f.supp_name);
                        $('#withdrawAmount1').focus();
                        if ($('#withdrawAmount1').val() >= 1) {
                            $('#withdrawAmount1').val();
                        } else {
                            $('#withdrawAmount1').val('1');
                        }
                    });
                },
                error: function() {
                    messageAlert("มีบางอย่างผิดพลาด !!");
                }
            });
        }
    });
    $('#withdrawList1').keydown(function(e) {
        if (e.keyCode == 13 || e.keyCode == 9) {
            $.ajax({
                url: url + 'stock/getStockWhereName/',
                type: 'POST',
                dataType: 'json',
                data: {
                    supp_name: $(this).val()
                },
                success: function(res) {
                    $.each(res, function(i, f) {
                        $('#withdrawListNum1').val(f.supp_no);
                        if ($('#withdrawAmount1').val() >= 1) {
                            $('#withdrawAmount1').val();
                            $('#withdrawAmount1').focus();
                        } else {
                            $('#withdrawAmount1').val('1');
                            $('#withdrawAmount1').focus();
                        }
                    });
                },
                error: function() {
                    messageAlert("มีบางอย่างผิดพลาด !!");
                }
            });
        }
    });
    $('#withdrawAmount1').keydown(function(e) {
        if (e.keyCode == 13) {
            if ($('#withdrawAmount1').val() >= 1) {
                $('#withdrawAmount1').val();
                $('#addwithdraw').trigger('click');
            } else {
                $('#withdrawAmount1').val('1');
                $('#addwithdraw').trigger('click');
            }
        }
    });
}

function addList(num = 0) {
    var num = $('.withdrawList').length;
    $("#addwithdraw").click(function() {
        if ($('#withdrawList1').val() == "") {
            messageAlert("ไม่มีรายการ !! ");
            $('#messageAlert').on('hide.bs.modal', function() {
               $('#withdrawList1').focus(); //working last modal hide
           });
        } else {
            var html = '<tr id="list' + (num + 1) + '">';
            html += '<td>';
            html += ' <input type="text" class="form-control form-control-sm withdrawListNum" name="withdrawListNum[]" id="withdrawListNum' + (num + 1) + '" value="' + $('#withdrawListNum1').val() + '" readonly>';
            html += ' </td>';
            html += '<td>';
            html += '<input type="text" class="form-control form-control-sm withdrawList" name="withdrawList[]" id="withdrawList' + (num + 1) + '" value="' + $('#withdrawList1').val() + '" readonly>';
            html += ' </td>';
            html += '<td>';
            html += '<input type="number" min="1" class="form-control form-control-sm withdrawAmount" name="withdrawAmount[]" id="withdrawAmount' + (num + 1) + '" value="' + $('#withdrawAmount1').val() + '" >';
            html += ' </td>';
            html += '<td class="text-center"> <button type="button" class="btn btn-danger btn-sm float-right delwithdraw" id="delwithdraw' + (num + 1) + '" name="delwithdraw[]"><i class="fa fa-minus"></i></button> </td>';
            html += '</tr>';
            /*----------  end list  ----------*/
            // console.log(num);
            $('#list1').remove(); //ลบ td ก่อนหน้า
            $('#tbody').append(html); //add td show add list withdraw
            $('#withdrawList1').val('');
            $('#withdrawAmount1').val('');
            $('#withdrawListNum1').val('').focus();
            delList(num + 1);
            countList(num + 1);
            $('.list').animate({
                scrollTop: $('.list')[0].scrollHeight
            }, 1000); //auto scroll height
            num++;
        }
    });
}

function autocomplete() {
    var suppName = [];
    var suppNo = [];
    $.ajax({
        url: url + 'stock/getStockAll/',
        dataType: 'json',
        cache: false,
        success: function(rs) {
            $.each(rs, function(i, f) {
                suppName.push(f.supp_name);
                suppNo.push(f.supp_no);
            });
            $('#withdrawListNum1').autocomplete({
                source: suppNo
            });
            $('#withdrawList1').autocomplete({
                source: suppName
            });
        },
        error: function() {
            messageAlert('มีบางอย่างผิดพลาด!!');
        }
    });
}

function submitReset() {
    /* button click */
    $('.submit').click(function() {
        // $('#form').submit();
        // exit();
        if (!$('#user_withdraw').val()) {
            $('#user_withdraw').focus();
        } else {
            if ($('#checkPrint').is(':checked')) { //check checked button print
                $('#tbheader').removeAttr("style");
                $('#repipient_user').text($('#user_withdraw option:selected').text());
                printData("showlist"); //print report withdraw
            }
            $.ajax({
                url: url + ctl + "/submit/",
                type: 'POST',
                dataType: 'html',
                data: $("#form").serializeArray(),
                success: function() {
                    messageAlert('บันทึกข้อมูลเรียบร้อย'); //show last submit success
                    $('#messageAlert').on('hide.bs.modal', function() {
                        location.reload(); //working last modal hide
                    });
                },
                error: function() {
                    messageAlert('มีบางอย่างผิดพลาด !!!');
                    // console.log('error'+ err);
                }
            });
        }
    });
    $('.reset').click(function() {
        location.reload();
    });
    /* ./ end button submit || reset */
}

function countList(num) {
    // console.log(num);
    for (i = 2; i < num; i++) {
        if ($('#withdrawList' + i).val() == $('#withdrawList' + num).val()) {
            $('#withdrawAmount' + i).val(Number($('#withdrawAmount' + i).val()) + Number($('#withdrawAmount' + num).val()));
            $('#list' + num).remove();
        }
    }
}

function delList(num) {
    $('#delwithdraw' + num).click(function() {
        var conf = confirm("ต้องการลบรายการ !");
        if (conf == true) {
            $('#list' + num).remove();
        } else {
            return false;
        }
    });
}

function printData(print_data) {
    var divToPrint = document.getElementById(print_data);
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}