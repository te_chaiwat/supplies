var $ = jQuery;
$(document).ready(function() {
    DataTable();
    configFontThai();
});

function configFontThai() {
    //config font thai to pdf
    pdfMake.fonts = {
        THSarabun: {
            normal: 'TH SarabunPSK.ttf',
            bold: 'TH SarabunPSK-Bold.ttf',
            italics: 'TH SarabunPSK-Italic.ttf',
            bolditalics: 'TH SarabunPSK-BoldItalic.ttf'
        }
    }
    //end config font thai to pdf
}

function DataTable() {
    var table = $('#tableData').DataTable({
        // dom: 'Bfrtip',
        // lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        // lengthChange: true,
        // "sPaginationType": "full_numbers",
        pagingType: 'full',
        // "order": [[ 0, 'asc' ]],
        "autoFill": false,
        "ordering": true,
        "searching": true,
        "info": false,
        "paging": false,
        "processing": true,
        "columns": [{
            "width": "5%",
        }, {
            "width": "20%"
        }, {
            "width": "35%"
        }, {
            "width": "10%",
            "sClass": "center"
        }, {
            "width": "20%"
        }, ],
        lengthMenu: [
            [-1],
            ['Show all']
        ],
        responsive: true,
        language: {
            "zeroRecords": "============== ไม่พบข้อมูลที่ค้นหา ==============",
            "sEmptyTable": "ไม่มีข้อมูลในตาราง",
            "sSearch": "ค้นหา: ",
        },
        buttons: [{
            extend: 'copy',
            text: '<i class="fa fa-files-o "></i> copy ',
            titleAttr: 'copy',
            className: 'border btn btn-sm'
        }, {
            extend: 'excel',
            text: '<i class="fa fa-file-excel-o"></i> excel',
            titleAttr: 'excel',
            className: 'border btn btn-sm'
        }, {
            extend: 'pdfHtml5',
            text: '<i class="fa fa-file-pdf-o"></i> pdf',
            titleAttr: 'pdf',
            className: 'border btn btn-sm',
            "pageSize": 'A4',
            "customize": function(doc) {
                //กำหนด style หลัก
                doc.defaultStyle = {
                    font: 'THSarabun',
                    fontSize: 16
                };
                //set with header to colum
                // doc.content[1].table.withs = [50,'auto', '*', '*'];
                // doc.style.tableHeader.fontSize = 16; //confit font size
            }
        }, {
            extend: 'print',
            text: '<i class="fa fa-print"></i> print',
            titleAttr: 'print',
            className: 'border btn btn-sm'
        }, ]
    });
    table.buttons().container().appendTo('#button-tolls');
}